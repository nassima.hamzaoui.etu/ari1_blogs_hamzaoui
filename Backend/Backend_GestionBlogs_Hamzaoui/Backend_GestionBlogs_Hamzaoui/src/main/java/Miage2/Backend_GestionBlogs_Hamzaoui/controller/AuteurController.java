package Miage2.Backend_GestionBlogs_Hamzaoui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

import Miage2.Backend_GestionBlogs_Hamzaoui.dto.AuteurDto;
import Miage2.Backend_GestionBlogs_Hamzaoui.service.AuteurService;



@RestController
@CrossOrigin(origins="http://localhost:3000", allowedHeaders = "*")
@RequestMapping("/auteurs")
public class AuteurController {

    @Autowired
    private AuteurService auteurService;

    @GetMapping("/all")
    public List<AuteurDto> getAllAuteurs() {
        return auteurService.getAllAuteurs();
    }

    @PostMapping("/create")
    public AuteurDto createAuteur(@RequestBody AuteurDto auteurDTO) {
        return auteurService.createAuteur(auteurDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AuteurDto> updateAuteur(@PathVariable Long id, @RequestBody AuteurDto updatedAuteurDTO) {
        Optional<AuteurDto> existingAuteurOptional = auteurService.getAuteurById(id);

        if (existingAuteurOptional.isPresent()) {
            AuteurDto existingAuteurDTO = existingAuteurOptional.get();
            existingAuteurDTO.setNom(updatedAuteurDTO.getNom());

            AuteurDto savedAuteurDTO = auteurService.createAuteur(existingAuteurDTO);

            return new ResponseEntity<>(savedAuteurDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void deleteAuteur(@PathVariable Long id) {
        auteurService.deleteAuteurById(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AuteurDto> getAuteurById(@PathVariable Long id) {
        Optional<AuteurDto> auteurOptional = auteurService.getAuteurById(id);
        return auteurOptional.map(auteurDTO -> new ResponseEntity<>(auteurDTO, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
