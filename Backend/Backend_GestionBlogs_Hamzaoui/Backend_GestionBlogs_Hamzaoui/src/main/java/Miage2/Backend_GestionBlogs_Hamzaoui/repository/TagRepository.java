package Miage2.Backend_GestionBlogs_Hamzaoui.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import Miage2.Backend_GestionBlogs_Hamzaoui.entity.Tag;

public interface TagRepository extends JpaRepository <Tag, Long> {
    
}
