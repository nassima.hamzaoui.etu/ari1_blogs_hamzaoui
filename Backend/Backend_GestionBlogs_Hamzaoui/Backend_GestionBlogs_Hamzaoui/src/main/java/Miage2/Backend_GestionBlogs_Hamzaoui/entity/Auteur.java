
package Miage2.Backend_GestionBlogs_Hamzaoui.entity;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class Auteur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String prenom;
    @Column
    private String nom;

 

    public Long getId() {
        return id;
    }



    public void setId(Long id) {
        this.id = id;
    }



    public String getPrenom() {
        return prenom;
    }



    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }



    public String getNom() {
        return nom;
    }



    public void setNom(String nom) {
        this.nom = nom;
    }



    public List<Blog> getBlogs() {
        return blogs;
    }



    public void setBlogs(List<Blog> blogs) {
        this.blogs = blogs;
    }



    @OneToMany(mappedBy = "auteur" , cascade = CascadeType.ALL)
    private List<Blog> blogs;


}
