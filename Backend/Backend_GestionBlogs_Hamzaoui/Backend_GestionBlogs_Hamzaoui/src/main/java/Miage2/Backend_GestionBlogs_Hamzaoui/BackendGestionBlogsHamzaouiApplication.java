package Miage2.Backend_GestionBlogs_Hamzaoui;

import java.util.*;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import Miage2.Backend_GestionBlogs_Hamzaoui.entity.Auteur;
import Miage2.Backend_GestionBlogs_Hamzaoui.entity.Blog;
import Miage2.Backend_GestionBlogs_Hamzaoui.entity.Tag;
import Miage2.Backend_GestionBlogs_Hamzaoui.repository.AuteurRepository;
import Miage2.Backend_GestionBlogs_Hamzaoui.repository.BlogRepository;
import Miage2.Backend_GestionBlogs_Hamzaoui.repository.TagRepository;

@SpringBootApplication
public class BackendGestionBlogsHamzaouiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendGestionBlogsHamzaouiApplication.class, args);
	}

	@Bean
    public CommandLineRunner initData(AuteurRepository auteurRepository, TagRepository tagRepository, BlogRepository blogRepository) {
        return args -> {
           
            Auteur auteur1 = new Auteur();
            auteur1.setPrenom("Nassima");
            auteur1.setNom("Hamzaoui");

            Auteur auteur2 = new Auteur();
            auteur2.setPrenom("Test");
            auteur2.setNom("Test2");

          
            Tag tag1 = new Tag();
            tag1.setNom("Tag 1");

            Tag tag2 = new Tag();
            tag2.setNom("Tag 2");

            Blog blog1 = new Blog();
            blog1.setMessage("blog 1");
            blog1.setDateCreation(new Date());
            blog1.setDateFin(new Date());
            blog1.setAuteur(auteur1);
            blog1.setTags(Arrays.asList(tag1));

            Blog blog2 = new Blog();
            blog2.setMessage("blog 2");
            blog2.setDateCreation(new Date());
            blog2.setDateFin(new Date());
            blog2.setAuteur(auteur2);
            blog2.setTags(Arrays.asList(tag2));

            auteurRepository.save(auteur1);
            auteurRepository.save(auteur2);
            tagRepository.save(tag1);
            tagRepository.save(tag2);
            blogRepository.save(blog1);
            blogRepository.save(blog2);

           
        };
    }
}