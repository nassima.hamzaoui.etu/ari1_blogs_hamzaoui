package Miage2.Backend_GestionBlogs_Hamzaoui.service;

import java.util.*;

import Miage2.Backend_GestionBlogs_Hamzaoui.dto.BlogDto;
import Miage2.Backend_GestionBlogs_Hamzaoui.entity.Auteur;
import Miage2.Backend_GestionBlogs_Hamzaoui.entity.Blog;
import Miage2.Backend_GestionBlogs_Hamzaoui.entity.Tag;


public interface BlogService {

  
  

   List<BlogDto> getAllBlogs();
   Optional<BlogDto> getBlogById(Long id);
   void deleteBlogById(Long id);
   BlogDto createBlog(BlogDto existingBlogDTO);
   BlogDto updateBlog(Long id, BlogDto updatedBlogDto);
}
