package Miage2.Backend_GestionBlogs_Hamzaoui.entity;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;

import java.util.*;

@Entity
public class Tag {

    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String nom;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Tag() {
    }

    public Tag(Long id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    @ManyToMany(mappedBy = "tags")
    private List <Blog> blogs;

}