package Miage2.Backend_GestionBlogs_Hamzaoui.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import Miage2.Backend_GestionBlogs_Hamzaoui.dto.BlogDto;
import Miage2.Backend_GestionBlogs_Hamzaoui.entity.Blog;
import Miage2.Backend_GestionBlogs_Hamzaoui.service.BlogService;

import java.util.*;

@CrossOrigin(origins="http://localhost:3000", allowedHeaders = "*")
@RestController
@RequestMapping("/blogs")
public class BlogController  {
    
        @Autowired
        private BlogService blogService;
    
        @GetMapping("/all")
        public List<BlogDto> getAllBlogs() {
            return blogService.getAllBlogs();
        }
    
        @PostMapping("/create")
        public BlogDto createBlog(@RequestBody BlogDto blogDTO) {
            return blogService.createBlog(blogDTO);
        }
    
        @PutMapping("/{id}")
        public ResponseEntity<BlogDto> updateBlog(@PathVariable Long id, @RequestBody BlogDto updatedBlogDTO) {
            Optional<BlogDto> existingBlogOptional = blogService.getBlogById(id);
            if (existingBlogOptional.isPresent()) {
                BlogDto existingBlogDTO = existingBlogOptional.get();
                existingBlogDTO.setMessage(updatedBlogDTO.getMessage());
                existingBlogDTO.setDateCreation(updatedBlogDTO.getDateCreation());
                existingBlogDTO.setDateFin(updatedBlogDTO.getDateFin());
                BlogDto savedBlogDto = blogService.createBlog(existingBlogDTO);

                return new ResponseEntity<>(savedBlogDto, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }
    
        @DeleteMapping("/{id}")
        public void deleteBlog(@PathVariable Long id) {
            blogService.deleteBlogById(id);
        }
    
        @GetMapping("/{id}")
    public ResponseEntity<BlogDto> getBlogById(@PathVariable Long id) {
        Optional<BlogDto> blogOptional = blogService.getBlogById(id);
        return blogOptional.map(blogDTO -> new ResponseEntity<>(blogDTO, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
       
    }
    