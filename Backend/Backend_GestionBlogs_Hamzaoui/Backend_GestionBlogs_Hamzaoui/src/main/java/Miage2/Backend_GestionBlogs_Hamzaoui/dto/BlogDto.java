package Miage2.Backend_GestionBlogs_Hamzaoui.dto;

import java.util.*;

public class BlogDto {

    private Long id;
    private String message;
    private Date dateCreation;
    private Date dateFin;
    private Long auteurId;
    private List<Long> tagIds;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public Date getDateCreation() {
        return dateCreation;
    }
    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }
    public Date getDateFin() {
        return dateFin;
    }
    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }
    public Long getAuteurId() {
        return auteurId;
    }
    public void setAuteurId(Long auteurId) {
        this.auteurId = auteurId;
    }
    public List<Long> getTagIds() {
        return tagIds;
    }
    public void setTagIds(List<Long> tagIds) {
        this.tagIds = tagIds;
    }
    @Override
    public String toString() {
        return "BlogDto [id=" + id + ", message=" + message + ", dateCreation=" + dateCreation + ", dateFin=" + dateFin
                + ", auteurId=" + auteurId + ", tagIds=" + tagIds + "]";
    }
  
   

}
