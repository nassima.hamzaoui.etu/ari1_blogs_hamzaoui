package Miage2.Backend_GestionBlogs_Hamzaoui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;
import Miage2.Backend_GestionBlogs_Hamzaoui.dto.TagDto;
import Miage2.Backend_GestionBlogs_Hamzaoui.service.TagService;

@RestController
@CrossOrigin(origins="http://localhost:3000",  allowedHeaders = "*")
@RequestMapping("/tags")
public class TagController {
    
    @Autowired
    private TagService tagService;

    @GetMapping("/all")
    public List<TagDto> getAllTags() {
        return tagService.getAllTags();
    }

    @PostMapping("/create")
    public TagDto createTag(@RequestBody TagDto tagDTO) {
        return tagService.createTag(tagDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TagDto> updateTag(@PathVariable Long id, @RequestBody TagDto updatedTagDTO) {
        Optional<TagDto> existingTagOptional = tagService.getTagById(id);

        if (existingTagOptional.isPresent()) {
            TagDto existingTagDTO = existingTagOptional.get();
            existingTagDTO.setNom(updatedTagDTO.getNom());

            TagDto savedTagDTO = tagService.createTag(existingTagDTO);

            return new ResponseEntity<>(savedTagDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void deleteTag(@PathVariable Long id) {
        tagService.deleteTagById(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TagDto> getTagById(@PathVariable Long id) {
        Optional<TagDto> tagOptional = tagService.getTagById(id);
        return tagOptional.map(tagDTO -> new ResponseEntity<>(tagDTO, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
