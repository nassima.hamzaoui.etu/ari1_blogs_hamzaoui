package Miage2.Backend_GestionBlogs_Hamzaoui.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Miage2.Backend_GestionBlogs_Hamzaoui.dto.TagDto;
import Miage2.Backend_GestionBlogs_Hamzaoui.entity.Tag;
import java.util.stream.Collectors;
import Miage2.Backend_GestionBlogs_Hamzaoui.repository.TagRepository;
import java.util.*;


@Service
public class TagServiceImpl implements TagService {
    


     @Autowired
    private TagRepository tagRepository;

    @Override
    public TagDto createTag(TagDto tagDTO) {
        Tag tag = convertToEntity(tagDTO);
        Tag savedTag = tagRepository.save(tag);
        return convertToDTO(savedTag);
    }

    @Override
    public List<TagDto> getAllTags() {
        List<Tag> tags = (List<Tag>) tagRepository.findAll();
        return convertToDTOList(tags);
    }

    @Override
    public Optional<TagDto> getTagById(Long id) {
        Optional<Tag> optionalTag = tagRepository.findById(id);
        return optionalTag.map(this::convertToDTO);
    }

    @Override
    public void deleteTagById(Long id) {
        tagRepository.deleteById(id);
    }

    private TagDto convertToDTO(Tag tag) {
        TagDto tagDto = new TagDto();
        tagDto.setId(tag.getId());
        tagDto.setNom(tag.getNom());
        return tagDto;
    }

    private List<TagDto> convertToDTOList(List<Tag> tags) {
        return tags.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    private Tag convertToEntity(TagDto tagDto) {
        Tag tag = new Tag();
        tag.setId(tagDto.getId());
        tag.setNom(tagDto.getNom());
        return tag;
    }
}
