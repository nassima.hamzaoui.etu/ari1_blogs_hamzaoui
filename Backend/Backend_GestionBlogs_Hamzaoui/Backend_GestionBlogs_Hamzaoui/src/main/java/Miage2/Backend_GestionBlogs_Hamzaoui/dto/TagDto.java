package Miage2.Backend_GestionBlogs_Hamzaoui.dto;

public class TagDto {

    private Long id;
    private String nom;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    @Override
    public String toString() {
        return "TagDto [id=" + id + ", nom=" + nom + "]";
    }

    
    
}
