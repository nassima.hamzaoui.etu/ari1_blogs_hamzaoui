package Miage2.Backend_GestionBlogs_Hamzaoui.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import Miage2.Backend_GestionBlogs_Hamzaoui.entity.Blog;

public interface BlogRepository  extends JpaRepository<Blog, Long>{


}
