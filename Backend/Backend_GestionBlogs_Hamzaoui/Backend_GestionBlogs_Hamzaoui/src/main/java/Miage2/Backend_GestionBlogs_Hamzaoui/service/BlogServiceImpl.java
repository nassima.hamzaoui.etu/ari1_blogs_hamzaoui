package Miage2.Backend_GestionBlogs_Hamzaoui.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.stream.Collectors;
import Miage2.Backend_GestionBlogs_Hamzaoui.dto.BlogDto;
import Miage2.Backend_GestionBlogs_Hamzaoui.entity.Auteur;
import Miage2.Backend_GestionBlogs_Hamzaoui.entity.Blog;
import Miage2.Backend_GestionBlogs_Hamzaoui.entity.Tag;
import Miage2.Backend_GestionBlogs_Hamzaoui.repository.BlogRepository;

@Service
public class BlogServiceImpl implements BlogService {

   
@Autowired
    private BlogRepository blogRepository;

    @Override
    public BlogDto createBlog(BlogDto BlogDto) {
        Blog blog = convertToEntity(BlogDto);
        Blog savedBlog = blogRepository.save(blog);
        return convertToDTO(savedBlog);
    }

    @Override
    public List<BlogDto> getAllBlogs() {
        List<Blog> blogs = (List<Blog>) blogRepository.findAll();
        return convertToDTOList(blogs);
    }

    @Override
    public Optional<BlogDto> getBlogById(Long id) {
        Optional<Blog> optionalBlog = blogRepository.findById(id);
        return optionalBlog.map(this::convertToDTO);
    }

    @Override
    public void deleteBlogById(Long id) {
        blogRepository.deleteById(id);
    }


  
    private BlogDto convertToDTO(Blog blog) {
        BlogDto blogDTO = new BlogDto();
        blogDTO.setId(blog.getId());
        blogDTO.setMessage(blog.getMessage());
        blogDTO.setDateCreation(blog.getDateCreation());
        blogDTO.setDateFin(blog.getDateFin());
        return blogDTO;
    }

    private List<BlogDto> convertToDTOList(List<Blog> blogs) {
        return blogs.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    private Blog convertToEntity(BlogDto blogDTO) {
        Blog blog = new Blog();
        blog.setId(blogDTO.getId());
        blog.setMessage(blogDTO.getMessage());
        blog.setDateCreation(blogDTO.getDateCreation());
        blog.setDateFin(blogDTO.getDateFin());
        return blog;
    }


    public BlogDto updateBlog(Long id, BlogDto updatedBlogDto) {
        Optional<Blog> existingBlogOptional = blogRepository.findById(id);
        if (existingBlogOptional.isPresent()) {
            Blog existingBlog = existingBlogOptional.get();

            existingBlog.setMessage(updatedBlogDto.getMessage());
            existingBlog.setDateCreation(updatedBlogDto.getDateCreation());
            existingBlog.setDateFin(updatedBlogDto.getDateFin());
            
            Blog updatedBlog = blogRepository.save(existingBlog);
            
            return convertToDto(updatedBlog);
        } else {
            
            return null;
        }
    }
    

        private BlogDto convertToDto(Blog blog) {
            BlogDto blogDto = new BlogDto();
            blogDto.setId(blog.getId());
            blogDto.setMessage(blog.getMessage());
            blogDto.setDateCreation(blog.getDateCreation());
            blogDto.setDateFin(blog.getDateFin());
           
            return blogDto;
        }

}