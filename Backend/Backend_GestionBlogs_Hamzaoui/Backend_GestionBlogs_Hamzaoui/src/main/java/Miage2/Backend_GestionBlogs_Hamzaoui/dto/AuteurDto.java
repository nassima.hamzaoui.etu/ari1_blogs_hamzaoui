package Miage2.Backend_GestionBlogs_Hamzaoui.dto;

public class AuteurDto {


    private Long id;
    private String nom;
    private String prenom;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
    @Override
    public String toString() {
        return "AuteurDto [id=" + id + ", nom=" + nom + ", prenom=" + prenom + "]";
    }
   

    
    
    
}
