package Miage2.Backend_GestionBlogs_Hamzaoui.service;

import Miage2.Backend_GestionBlogs_Hamzaoui.dto.AuteurDto;
import java.util.*;

public interface AuteurService {

    
AuteurDto createAuteur(AuteurDto auteurDTO);

    List<AuteurDto> getAllAuteurs();

    Optional<AuteurDto> getAuteurById(Long id);

    void deleteAuteurById(Long id);
    
}
