
package Miage2.Backend_GestionBlogs_Hamzaoui.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.stream.Collectors;
import Miage2.Backend_GestionBlogs_Hamzaoui.dto.AuteurDto;
import Miage2.Backend_GestionBlogs_Hamzaoui.entity.Auteur;
import Miage2.Backend_GestionBlogs_Hamzaoui.repository.AuteurRepository;
import java.util.*;

@Service
public class AuteurServiceImpl implements AuteurService {

    @Autowired
    private AuteurRepository auteurRepository;

    @Override
    public AuteurDto createAuteur(AuteurDto auteurDTO) {
        Auteur auteur = convertToEntity(auteurDTO);
        Auteur savedAuteur = auteurRepository.save(auteur);
        return convertToDTO(savedAuteur);
    }

    @Override
    public List<AuteurDto> getAllAuteurs() {
        List<Auteur> auteurs = (List<Auteur>) auteurRepository.findAll();
        return convertToDTOList(auteurs);
    }

    @Override
    public Optional<AuteurDto> getAuteurById(Long id) {
        Optional<Auteur> optionalAuteur = auteurRepository.findById(id);
        return optionalAuteur.map(this::convertToDTO);
    }

    @Override
    public void deleteAuteurById(Long id) {
        auteurRepository.deleteById(id);
    }

    private AuteurDto convertToDTO(Auteur auteur) {
        AuteurDto auteurDto = new AuteurDto();
        auteurDto.setId(auteur.getId());
        auteurDto.setNom(auteur.getNom());
        auteurDto.setPrenom(auteur.getPrenom());
        return auteurDto;
    }


    private List<AuteurDto> convertToDTOList(List<Auteur> auteurs) {
        return auteurs.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    private Auteur convertToEntity(AuteurDto auteurDto) {
        Auteur auteur = new Auteur();
        auteur.setId(auteurDto.getId());
        auteur.setNom(auteurDto.getNom());
        auteur.setPrenom(auteurDto.getPrenom());
        return auteur;
    }

}