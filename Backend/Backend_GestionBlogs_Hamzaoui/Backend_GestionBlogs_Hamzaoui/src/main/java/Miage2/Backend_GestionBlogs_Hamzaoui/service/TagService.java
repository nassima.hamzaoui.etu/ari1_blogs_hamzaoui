package Miage2.Backend_GestionBlogs_Hamzaoui.service;

import Miage2.Backend_GestionBlogs_Hamzaoui.dto.TagDto;
import java.util.*;

public interface TagService {

    
  TagDto createTag(TagDto tagDTO);

    List<TagDto> getAllTags();

    Optional<TagDto> getTagById(Long id);

    void deleteTagById(Long id);
    
}
