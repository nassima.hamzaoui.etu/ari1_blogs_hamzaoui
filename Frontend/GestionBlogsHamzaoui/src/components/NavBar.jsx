import React from 'react';
import { Link } from 'react-router-dom';

const NavBar = () => {
  return (
    <nav className="flex justify-between text-xl font-bold bg-purple-800 p-4">
     
      
        <Link to="/auteurs" className="text-white hover:text-gray-300">Auteurs</Link>
        <Link to="/blogs" className="text-white hover:text-gray-300">Blogs</Link>
        <Link to="/tags" className="text-white hover:text-gray-300">Tags</Link>
      
    </nav>
  );
};

export default NavBar;
