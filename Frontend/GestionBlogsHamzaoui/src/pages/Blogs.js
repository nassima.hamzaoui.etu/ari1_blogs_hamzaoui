import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

const Blogs = () => {
  const [blogs, setBlogs] = useState([]);

  useEffect(() => {
    const fetchBlogs = async () => {
      try {
        const response = await axios.get('http://localhost:4047/blogs/all');
        setBlogs(response.data);
      } catch (error) {
        console.error('Error fetching blogs:', error);
      }
    };
    fetchBlogs();
  }, []);

  const handleDelete = async (id) => {
    try {
      await axios.delete(`http://localhost:4047/blogs/${id}`);
     
      setBlogs(blogs.filter(blog => blog.id !== id));
    } catch (error) {
      console.error('Error deleting blog:', error);
    }
  };

  return (
    <div className="container mx-auto mt-8 px-4">
      <div className="flex justify-between items-center mb-6">
        <h2 className="text-3xl font-bold text-purple-900">Liste des Blogs</h2>
        <Link to="/blogs/create">
          <button className="bg-purple-500 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded">
            Créer un Blog
          </button>
        </Link>
      </div>
      <ul className="grid grid-cols-1 gap-6">
        {blogs.map((blog) => (
          <li key={blog.id} className="bg-white rounded-lg shadow-md overflow-hidden">
            <div className="p-6">
              <div className="flex justify-between items-center">
                <p className="text-xl font-semibold text-gray-800">{blog.message}</p>
                <div className="text-gray-500 text-sm">
                  <p>Créé le {formatDate(blog.dateCreation)}</p>
                  <p>Fin le {formatDate(blog.dateFin)}</p>
                </div>
                <Link to={`/blogs/update/${blog.id}`}>
                  <button className="bg-purple-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                    Modifier
                  </button>
                </Link>
                <button
                  onClick={() => handleDelete(blog.id)}
                  className="bg-red-500 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded"
                >
                  Supprimer
                </button>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );

  function formatDate(dateString) {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, '0');
    const month = (date.getMonth() + 1).toString().padStart(2, '0'); 
    const year = date.getFullYear();
    return `${month}-${day}-${year}`;
  }
};

export default Blogs;
