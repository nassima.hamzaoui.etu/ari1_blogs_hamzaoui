import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';

const UpdateTag = () => {
  const { id } = useParams();
  const [tag, setTag] = useState({});
  const [formData, setFormData] = useState({ nom: '' });

  useEffect(() => {
    const fetchTag = async () => {
      try {
        const response = await axios.get(`http://localhost:4047/tags/${id}`);
        setTag(response.data);
        setFormData({ nom: response.data.nom });
      } catch (error) {
        console.error('Error fetching tag:', error);
      }
    };
    fetchTag();
  }, [id]);

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await axios.put(`http://localhost:4047/tags/${tag.id}`, formData);
      // Rediriger l'utilisateur vers la liste des tags après la mise à jour
      window.location.href = '/tags';
    } catch (error) {
      console.error('Error updating tag:', error);
    }
  };

  return (
    <div className="container mx-auto mt-8 px-4">
      <h2 className="text-3xl font-bold text-purple-900">Modifier le Tag</h2>
      <form onSubmit={handleSubmit} className="mt-4">
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="nom">
            Nom du Tag
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="nom"
            name="nom"
            type="text"
            placeholder="Nom du Tag"
            value={formData.nom}
            onChange={handleChange}
          />
        </div>
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          type="submit"
        >
          Mettre à Jour
        </button>
      </form>
    </div>
  );
};

export default UpdateTag;
