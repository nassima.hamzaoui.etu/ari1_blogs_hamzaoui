import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';

const UpdateAuteur = () => {
  const { id } = useParams();
  const [auteur, setAuteur] = useState({});
  const [formData, setFormData] = useState({
    prenom: '',
    nom: '',
  });

  useEffect(() => {
    const fetchAuteur = async () => {
      try {
        const response = await axios.get(`http://localhost:4047/auteurs/${id}`);
        setAuteur(response.data);
        setFormData({
          prenom: response.data.prenom,
          nom: response.data.nom,
        });
      } catch (error) {
        console.error('Error fetching auteur:', error);
      }
    };
    fetchAuteur();
  }, [id]);

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await axios.put(`http://localhost:4047/auteurs/${id}`, formData);
      
      window.location.href = '/auteurs';
    } catch (error) {
      console.error('Error updating auteur:', error);
    }
  };

  return (
    <div className="container mx-auto mt-8 px-4">
      <h2 className="text-3xl font-bold text-purple-900">Modifier l'Auteur</h2>
      <form onSubmit={handleSubmit} className="mt-4">
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="prenom">
            Prénom
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="prenom"
            name="prenom"
            type="text"
            placeholder="Prénom"
            value={formData.prenom}
            onChange={handleChange}
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="nom">
            Nom
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="nom"
            name="nom"
            type="text"
            placeholder="Nom"
            value={formData.nom}
            onChange={handleChange}
          />
        </div>
        <button
          className="bg-purple-500 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded"
          type="submit"
        >
          Mettre à Jour
        </button>
      </form>
    </div>
  );
};

export default UpdateAuteur;
