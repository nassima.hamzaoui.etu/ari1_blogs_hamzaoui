import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

const CreateAuteur = () => {
  const initialValues = {
    prenom: '',
    nom: '',
  };

  const validationSchema = Yup.object({
    prenom: Yup.string().required('Le prénom est requis'),
    nom: Yup.string().required('Le nom est requis'),
  });

  const handleSubmit = async (values, { setSubmitting, resetForm }) => {
    try {
      const response = await fetch('http://localhost:4047/auteurs/create', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(values),
      });

      if (!response.ok) {
        throw new Error('Erreur lors de la création de l\'auteur');
      }

      console.log('Auteur créé avec succès');

      resetForm();
    } catch (error) {
      console.error('Erreur lors de la création de l\'auteur:', error.message);
      alert('Une erreur est survenue lors de la création de l\'auteur. Veuillez réessayer.');
    } finally {
      setSubmitting(false);
    }
  };

  return (
    <div className="mx-auto mt-8 max-w-2xl">
      <h2 className="text-3xl font-bold mb-6 text-center">Créer un Auteur</h2>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        <Form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
          <div className="mb-4">
            <label htmlFor="prenom" className="block text-gray-700 text-sm font-bold mb-2">Prénom</label>
            <Field
              id="prenom"
              name="prenom"
              type="text"
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
            <ErrorMessage name="prenom" component="div" className="text-red-500 text-sm" />
          </div>
          <div className="mb-4">
            <label htmlFor="nom" className="block text-gray-700 text-sm font-bold mb-2">Nom</label>
            <Field
              id="nom"
              name="nom"
              type="text"
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
            <ErrorMessage name="nom" component="div" className="text-red-500 text-sm" />
          </div>
          <div className="flex items-center justify-center">
            <button type="submit" className="bg-purple-500 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
              Créer Auteur
            </button>
          </div>
        </Form>
      </Formik>
    </div>
  );
};

export default CreateAuteur;
