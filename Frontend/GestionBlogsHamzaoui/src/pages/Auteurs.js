import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

const Auteurs = () => {
  const [auteurs, setAuteurs] = useState([]);

  useEffect(() => {
    const fetchAuteurs = async () => {
      try {
        const response = await axios.get('http://localhost:4047/auteurs/all');
        setAuteurs(response.data);
      } catch (error) {
        console.error('Error fetching auteurs:', error);
      }
    };
    fetchAuteurs();
  }, []);

  const handleDelete = async (id) => {
    try {
      await axios.delete(`http://localhost:4047/auteurs/${id}`);
      
      setAuteurs(auteurs.filter(auteur => auteur.id !== id));
    } catch (error) {
      console.error('Error deleting auteur:', error);
    }
  };

  const handleUpdate = async (id) => {
    
    window.location.href = `/auteurs/update/${id}`;
  };

  return (
    <div className="container mx-auto mt-8 px-4">
      <div className="max-w-4xl mx-auto flex justify-between items-center mb-6">
        <h2 className="text-3xl font-bold text-purple-900 mb-4">Liste des Auteurs</h2>
        <Link to="/auteurs/create">
          <button className="bg-purple-500 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded">
            Créer un Auteur
          </button>
        </Link>
      </div>
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
        {auteurs.map((auteur) => (
          <div key={auteur.id} className="bg-purple-100 border border-purple-300 rounded-lg shadow-md overflow-hidden">
            <div className="flex items-center justify-between p-4">
              <h3 className="text-xl font-semibold text-gray-800">{auteur.prenom} {auteur.nom}</h3>
              <div>
                <button
                  onClick={() => handleUpdate(auteur.id)}
                  className="bg-purple-500 hover:bg-purple-700 text-white text-sm font-bold py-1 px-2 rounded mr-2"
                >
                  Modifier
                </button>
                <button
                  onClick={() => handleDelete(auteur.id)}
                  className="bg-red-500 hover:bg-purple-700 text-white text-sm font-bold py-1 px-2 rounded"
                >
                  Supprimer
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Auteurs;
