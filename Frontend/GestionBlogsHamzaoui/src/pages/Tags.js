import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

const Tags = () => {
  const [tags, setTags] = useState([]);

  useEffect(() => {
    const fetchTags = async () => {
      try {
        const response = await axios.get('http://localhost:4047/tags/all');
        setTags(response.data);
      } catch (error) {
        console.error('Error fetching tags:', error);
      }
    };
    fetchTags();
  }, []);

  const handleDelete = async (id) => {
    try {
      await axios.delete(`http://localhost:4047/tags/${id}`);
      
      setTags(tags.filter(tag => tag.id !== id));
    } catch (error) {
      console.error('Error deleting tag:', error);
    }
  };

  return (
    <div className="container mx-auto mt-8 px-4">
      <div className="flex justify-between items-center mb-6">
        <h2 className="text-3xl font-bold text-purple-900">Liste des Tags</h2>
        <Link to="/tags/create">
          <button className="bg-purple-500 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded">
            Créer un Tag
          </button>
        </Link>
      </div>
      <ul className="grid grid-cols-1 gap-6">
        {tags.map((tag) => (
          <li key={tag.id} className="bg-white rounded-lg shadow-md overflow-hidden">
            <div className="p-6">
              <div className="flex justify-between items-center">
                <p className="text-xl font-semibold text-gray-800">{tag.nom}</p>
                <div>
                  <Link to={`/tags/update/${tag.id}`}>
                    <button className="bg-purple-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-2">
                      Modifier
                    </button>
                  </Link>
                  <button
                    onClick={() => handleDelete(tag.id)}
                    className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
                  >
                    Supprimer
                  </button>
                </div>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Tags;
