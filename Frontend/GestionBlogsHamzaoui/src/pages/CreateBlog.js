import React, { useEffect, useState } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';

const CreateBlog = () => {
  
    const [auteurs, setAuteurs] = useState([]);
    const [tags, setTags] = useState([]);
  
    useEffect(() => {
      axios.get('http://localhost:4047/auteurs/all')
        .then(response => {
          setAuteurs(response.data);
        })
        .catch(error => {
          console.error('Erreur lors de la récupération des auteurs:', error);
        });
  
      axios.get('http://localhost:4047/tags/all')
        .then(response => {
          setTags(response.data);
        })
        .catch(error => {
          console.error('Erreur lors de la récupération des tags:', error);
        });
    }, []);
  
    const handleSubmit = async (values) => {
      try {
        const payload = {
          message: values.message,
          dateCreation: values.dateCreation,
          dateFin: values.dateFin,
          auteurId: values.selectedAuteur, 
          tagId: values.selectedTags,
        };
  
        const response = await axios.post('http://localhost:4047/blogs/create', payload);
        console.log('Blog créé avec succès');
      } catch (error) {
        console.error('Erreur lors de la création du blog:', error.message);
        alert('Une erreur est survenue lors de la création du blog. Veuillez réessayer.');
      }
    };
  
    const initialValues = {
      message: '',
      dateCreation: '',
      dateFin: '',
      selectedAuteur: '',
      selectedTags: [],
    };
  
    const validationSchema = Yup.object({
      message: Yup.string().required('Le message est requis'),
      dateCreation: Yup.date().required('La date de création est requise'),
      dateFin: Yup.date().required('La date de fin est requise'),
      selectedAuteur: Yup.string().required("L'auteur est requis"),
      /*selectedTags: Yup.array().min(1, 'Sélectionnez au moins un tag'),*/
    });
  
    return (
      <div className="mx-auto mt-8 max-w-2xl">
        <h2 className="text-3xl font-bold mb-6 text-center">Créer un Blog</h2>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={handleSubmit}
        >
          <Form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
  <div className="mb-4">
    <label htmlFor="message" className="block text-gray-700 text-sm font-bold mb-2">Message</label>
    <Field
      id="message"
      name="message"
      as="textarea"
      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
      rows="4"
    />
    <ErrorMessage name="message" component="div" className="text-red-500 text-sm" />
  </div>
  <div className="mb-4">
    <label htmlFor="dateCreation" className="block text-gray-700 text-sm font-bold mb-2">Date de création</label>
    <Field
      id="dateCreation"
      name="dateCreation"
      type="date"
      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
    />
    <ErrorMessage name="dateCreation" component="div" className="text-red-500 text-sm" />
  </div>
  <div className="mb-6">
    <label htmlFor="dateFin" className="block text-gray-700 text-sm font-bold mb-2">Date de fin</label>
    <Field
      id="dateFin"
      name="dateFin"
      type="date"
      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
    />
    <ErrorMessage name="dateFin" component="div" className="text-red-500 text-sm" />
  </div>
  <div className="mb-4">
    <label htmlFor="selectedAuteur" className="block text-gray-700 text-sm font-bold mb-2">Auteur</label>
    <Field
      as="select"
      id="selectedAuteur"
      name="selectedAuteur"
      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
    >
      <option value="">Sélectionner un auteur</option>
      {auteurs.map((auteur) => (
        <option key={auteur.id} value={auteur.id}>
          {auteur.nom}
        </option>
      ))}
    </Field>
    <ErrorMessage name="selectedAuteur" component="div" className="text-red-500 text-sm" />
  </div>
  <div className="mb-4">
    <label className="block text-gray-700 text-sm font-bold mb-2">Tags</label>
    {tags.map((tag) => (
      <label key={tag.id} className="inline-flex items-center">
        <Field
          type="checkbox"
          name="selectedTags"
          value={tag.id} 
          className="form-checkbox h-5 w-5 text-gray-600"
        />
        <span className="ml-2">{tag.nom}</span>
      </label>
    ))}
    <ErrorMessage name="selectedTags" component="div" className="text-red-500 text-sm" />
  </div>
  <div className="flex items-center justify-center">
    <button type="submit" className="bg-purple-500 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
      Créer Blog
    </button>
  </div>
</Form>

        </Formik>
      </div>

      
    );
  };
  
  export default CreateBlog;
  
