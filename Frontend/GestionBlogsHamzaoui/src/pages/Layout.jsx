
import * as React from 'react';
import { Link, Outlet } from 'react-router-dom';
import  NavBar  from '../components/NavBar';

export const Layout = () => {
    return (
        <>
            <NavBar/>
            <main className="py-4">
                <Outlet />
            </main>
        </>
    );
};