import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';

const UpdateBlog = () => {
  const { id } = useParams();
  const [blog, setBlog] = useState({});
  const [formData, setFormData] = useState({
    message: '',
    dateCreation: '',
    dateFin: '',
  });

  useEffect(() => {
    const fetchBlog = async () => {
      try {
        const response = await axios.get(`http://localhost:4047/blogs/${id}`);
        setBlog(response.data);
        setFormData({
          message: response.data.message,
          dateCreation: response.data.dateCreation,
          dateFin: response.data.dateFin,
        });
      } catch (error) {
        console.error('Error fetching blog:', error);
      }
    };
    fetchBlog();
  }, [id]);

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.put(`http://localhost:4047/blogs/${id}`, formData, {
        headers: {
          'Content-Type': 'application/json',
        }
      });
      
      window.location.href = '/blogs';
    } catch (error) {
      console.error('Error updating blog:', error);
      
    }
  };

  return (
    <div className="container mx-auto mt-8 px-4">
      <h2 className="text-3xl font-bold text-purple-900">Modifier le Blog</h2>
      <form onSubmit={handleSubmit} className="mt-4">
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="message">
            Message
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="message"
            name="message"
            type="text"
            placeholder="Message"
            value={formData.message}
            onChange={handleChange}
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="dateCreation">
            Date de Création
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="dateCreation"
            name="dateCreation"
            type="text"
            placeholder="Date de Création"
            value={formData.dateCreation}
            onChange={handleChange}
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="dateFin">
            Date de Fin
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="dateFin"
            name="dateFin"
            type="text"
            placeholder="Date de Fin"
            value={formData.dateFin}
            onChange={handleChange}
          />
        </div>
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          type="submit"
        >
          Mettre à Jour
        </button>
      </form>
    </div>
  );
};

export default UpdateBlog;
